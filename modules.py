# import random
# num=random.randint(100,200)
# print(num)
# from random import randint
# num=randint(100,200)
# print(num)

# import random as r                #rename the function of module using AS keyword
# num=r.randint(100,200)
# print(num)

from random import randint as r     #renaming specific function of a module
print(r(100,200))

print(__name__)