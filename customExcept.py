class MyError(Exception):
    def __init__(self,v):
        self.v=v
    def __str__(self):
        return self.v

try:
    print('code inside try block')
    raise MyError(404) 

except MyError as vl:
    print('code inside except block')
    print('ERROR: {} User Defined Exception'.format(vl.v))